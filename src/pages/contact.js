import React from "react"
import CSS from "../css/main-style.css"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import styled from "styled-components"
import Contactmod from "../components/contactmod"
import Form from "../components/form"





const IndexPage = () => (
  
  <Layout>
    <SEO title="Kingsite Friesland Duitsland" />
    <Contactmod />
    <Form />

  </Layout>

)

export default IndexPage