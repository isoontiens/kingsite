import React from "react"
import CSS from "../css/main-style.css"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import styled from "styled-components"
import Home from "../components/home"
import Events from "../components/Events"
import Nieuws from "../components/nieuws"
import Video from "../components/video"



const IndexPage = () => (
  
  <Layout>
    <SEO title="Grove Street: home" />
    <Nieuws />
    <Video />
  </Layout>

)

export default IndexPage