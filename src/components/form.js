import React, { Component } from 'react'
import styled from "styled-components"

const Container = styled.div`
    padding: 50px 0
`

const Form = styled.form`
    width: 100%;
    margin: 50px 0;

`
const Containerinput = styled.div`
    display: inline-block;
    width: 100%;
    background-color: rgba(0,0,0,0.04);
    margin: 4px;
    padding: 16px;
    

    
`
const Paragraph = styled.p`
    float: left;
    font-size: 1.5em;
    font-weight: 
`
const Inputfield = styled.input`
    float: right;
    background-color: none;
    width: 600px;
`
const Button = styled.input`
    background-color: #E1001A;
    padding: 20px;
    border: none;
    color: white;
    font-weight: 900;
    font-size: 1.8em;
    margin: 75px 0;
`


export default class form extends Component {
    render() {
        return (
            <Container>
                <h1>Persoonlijke Gegevens</h1>
            <Form>
                <Containerinput><Paragraph>Voornaam*</Paragraph><Inputfield type="text"></Inputfield></Containerinput>
                <Containerinput><Paragraph>Achternaam*</Paragraph><Inputfield type="text"></Inputfield></Containerinput>
                <Containerinput><Paragraph>Adres</Paragraph><Inputfield type="text"></Inputfield></Containerinput>
                <Containerinput><Paragraph>Postcode</Paragraph><Inputfield type="text"></Inputfield></Containerinput>
                <Containerinput><Paragraph>Plaats</Paragraph><Inputfield type="text"></Inputfield></Containerinput>
                <Containerinput><Paragraph>Land</Paragraph><Inputfield type="text"></Inputfield></Containerinput>
                <Containerinput><Paragraph>Telefoon</Paragraph><Inputfield type="text"></Inputfield></Containerinput>
                <Containerinput><Paragraph>Email</Paragraph><Inputfield type="text"></Inputfield></Containerinput>

                <Button type="button" name="submit" value="verzenden"></Button>
            </Form>
            </Container>
        )
    }
}
