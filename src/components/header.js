import PropTypes from "prop-types"
import React from "react"
import Navigation from "./navigation"
import Language from "./language"
import styled from "styled-components"
import { useStaticQuery, graphql } from "gatsby"
import Headerimg from "./header-img"

const Left = styled.div`
  display: inline-block;
  width: 70%;
`
const Right = styled.div`
  display: inline-block;
  width: 30%;
  text-align: center;
`
const Bottom = styled.div`
`
const Emptycontainer = styled.div`
  height: 100px;
  
`

const Headerflex = styled.div`
display: flex;
justify-content: space-between;
`

const Header = () => (
  <header>
 
    <Emptycontainer />
    <Headerflex>
    <Left>
       <Navigation>
       </Navigation>
    </Left>

    <Right>
       <Language>
       </Language>
    </Right>
    </Headerflex>
    <Bottom>
        <Headerimg />

    </Bottom>

  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
