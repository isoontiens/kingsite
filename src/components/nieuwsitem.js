
import React, { Component } from 'react'
import styled from "styled-components"
import NieuwsImg from "./nieuws-img"
import Img from "gatsby"

const Container = styled.div`
    padding: 50px
`

const EventP = styled.p`
    font-size: 0.9em;
    text-align: left;
`
const EventLink =styled.a`
    text-align: left;
`

const EventH2 =styled.h2`
    margin: 10px 0;
    text-align: left;
`
const Datum = styled.p`
padding: 10px;
font-size: 0.9em;
    
`
export default class Nieuwsitem extends Component {
    render() {
        return (
            <>
              <Datum>Geplaatst op 28 September 2019</Datum>
               <NieuwsImg />
               <Container>
                <EventH2>
               Hier komt de kop van Nieuws 1
                </EventH2>
                <EventP>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum </EventP>
                <EventLink>Lees verder</EventLink>
                </Container>
            </>
        )
    }
}
