import React, { Component } from 'react'
import styled from "styled-components"

const Container = styled.div`

`
const Column = styled.div`

`
const Mainlink = styled.a`
  color: white;
  display: inline-block;
  width: 100%;
  font-weight: 600;
`

const Secondarylink = styled.a`
  font-weight: 100;
  font-size: 0.9em;
  color: white;
  display: inline-block;
  width: 100%;
  margin: 0;
  padding: 0;

`
const Copyright = styled.div`
    width: 100%;
    display: inline-block; 
    padding: 10px; 
`

const Right = styled.nav`
    float: right;
    margin: none;
    padding: none;
`

const Left = styled.div`
    float: left;
    p{
        color: #9B9C9E;
        font-size: 0.9em;
    }
`
const Background = styled.div`
    @media screen and (max-width : 1024px){
        column-count: 3;
        column-gap: 20px;
    }
    @media screen and (max-width : 1200px){
        column-count: 3;
        column-gap: 20px;
    }
    @media screen and (max-width : 1400px){
        column-gap: 10px;
    }
    background-color: #9B9C9E;
    min-height: 200px;
    column-count: 6;
    column-gap: 40px;
    padding: 20px 50px;
    padding-left: 10%;

`
const Link = styled.a`
    margin: 0 10px ;
    color: #9B9C9E;
    font-size: 0.9em;
`

export default class footer extends Component {
    render() {
        return (
            <Container>
              <Background>
                <Column>
                    <Mainlink>Home</Mainlink>
                    <Secondarylink></Secondarylink>
                </Column>
                <Column>
                    <Mainlink>Nieuws</Mainlink>
                    <Secondarylink>Laatste nieuws</Secondarylink>
                    <Secondarylink>Foto's en video's</Secondarylink>
                </Column>
                <Column>
                    <Mainlink>Activiteiten</Mainlink>
                    <Secondarylink></Secondarylink>
                </Column>
                <Column>
                    <Mainlink>Over ons</Mainlink>
                    <Secondarylink></Secondarylink>
                </Column>
                <Column>
                    <Mainlink>Meedoen</Mainlink>
                    <Secondarylink></Secondarylink>
                </Column>
                <Column>
                    <Mainlink>Contact</Mainlink>
                    <Secondarylink></Secondarylink>
                 </Column>
               </Background>
               <Copyright>
                <Left><p>© Duitsland Friesland Kring - alle rechten voorbehouden</p></Left>
                    <Right>
                         <Link>Cookies</Link>
                         <Link>Privacy Statement</Link>
                         <Link>Disclaimer</Link>

                    </Right>    
                </Copyright>
              
            </Container>
        )
    }
}
