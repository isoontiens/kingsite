import { links } from "gatsby"
import React, { Component } from 'react'
import styled from "styled-components"

const Navcont = styled.nav`
  height: 40px;
  display: flex;
  justify-content: space-around;
`
const Navlink = styled.a`
  color: #9B9C9E;
`

export default class navigation extends Component {
    render() {
        return (
            <Navcont>
              <Navlink>Home</Navlink>
              <Navlink>Nieuws</Navlink>
              <Navlink>Actvitiveiten</Navlink>
              <Navlink>Over ons</Navlink>
              <Navlink>Contact</Navlink>
            </Navcont>
        )
    }
}
