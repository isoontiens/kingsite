import React, { Component } from 'react'
import styled from "styled-components"
import Event from "./event"
import EventImg from "./event-img"

const Eventcontainer = styled.div`
display: inline-block;
border: 2px solid lightgrey;
column-span: all;

`
const Container = styled.div`
padding: 50px 0
`


const Flex = styled.div`
    width: 100%;
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 10px;
    @media screen and (max-width: 960px) {
        grid-template-columns: repeat(2, 1fr);
    }
`

export default class Events extends Component {
    render() {
        return (
            <Container>
               <h1>Evenementen</h1>
               <Flex>
                 <Eventcontainer>
                    <Event />
                 </Eventcontainer>
                 <Eventcontainer>
                    <Event />
                 </Eventcontainer>
                 <Eventcontainer>
                    <Event />
                 </Eventcontainer>
                 <Eventcontainer>
                    <Event />
                 </Eventcontainer>
                </Flex>

            </Container>
        )
    }
}
