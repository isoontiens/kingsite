/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Header from "./header"
import "../css/main-style.css"
import "../css/layout.css"
import styled from "styled-components"
import Sidebar from "./sidebar"
import Footer from "./footer"
import Share from "./share"


const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)
  
  const Center = styled.div`
    margin: 0 auto;
    max-width: 1000px;
    min-height: 1000px;
  `


  const Main = styled.div`
    display: flex;
    flex-direction: row;
    margin: 0 50px;
  `

  const Left = styled.div`
  display: inline-block;
  width: calc(100% - 170px)
  `
  const Right = styled.div`
  display: inline-block;
  width: 170px;
  margin: 0 auto;


  

  `

  return (
    <>
    <Main>
    <Left>
      <Header siteTitle={data.site.siteMetadata.title} />
      <div>
        <Center className="pagecontent">
          {children}

        </Center>
        <Share />
      </div>
      <Footer />

     </Left>
    <Right>
        <Sidebar>
   
        </Sidebar>

      </Right>

      </Main>

     </>

  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
