import React, { Component } from 'react'
import styled from "styled-components"
import Nieuwsitem from "./nieuwsitem"
import EventImg from "./event-img"

const Container = styled.div`
    padding 50px 0;
`

const Eventcontainer = styled.div`
display: inline-block;
width: 450px;
border: 2px solid lightgrey;
`

const Flex = styled.div`
    display: flex;
    justify-content: space-around;
    margin: 100px 0;
`


export default class Nieuws extends Component {
    render() {
        return (
            <Container>
               <h1>Nieuws</h1>
               <Flex>
                 <Eventcontainer>
                 <Nieuwsitem />
                 </Eventcontainer>
                 <Eventcontainer>
                 <Nieuwsitem />
                 </Eventcontainer>
                </Flex>

            </Container>
        )
    }
}
