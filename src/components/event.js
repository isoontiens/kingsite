import React, { Component } from 'react'
import styled from "styled-components"
import EventImg from "./event-img"
import Img from "gatsby"

const Container = styled.div `
    padding: 20px;
    box-sizing: border-box;
`

const EventP = styled.p`
    font-size: 0.9em;
    text-align: left;
`
const EventLink =styled.a`
    text-align: left;
`

const EventH2 =styled.h2`
    margin: 10px 0;
    text-align: left;
`

export default class event extends Component {
    render() {
        return (
            <>
               <EventImg />
               <Container>
                <EventH2>
               Hier komt de kop van evenement 1
                </EventH2>
                <EventP>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum </EventP>
                <EventLink>Lees verder</EventLink>
                </Container>
            </>
        )
    }
}
