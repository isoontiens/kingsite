import React, { Component } from 'react'
import styled from "styled-components"


const Video = () => (
    <div className="video">
      <iframe width="800" height="500px" src="https://www.youtube.com/embed/4lbp9QtIp7Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  )
const Container = styled.div`
    margin: 0 auto;
    Padding: 50px 0;
`

export default class video extends Component {
    render() {
        return (


            <Container>
                <Video />
            </Container>
        )
    }
}
