import React, { Component } from 'react'
import styled from "styled-components"

const Container = styled.div`
    text-align: center;
    border-bottom: 2px solid black;
    padding: 50px 0;
`
const Headertext = styled.h1`
    color: #9B9C9E; 
    margin: 50px 0;
    font-size: 60px;
`

const Greyparagraph = styled.p`
 
`
const Link = styled.a`
  margin: 50px 0;
`


export default class Home extends Component {
    render() {
        return (


            <Container>
                
                    <Headertext>Beleef Duitsland!</Headertext>
                    <Greyparagraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quam mauris, placerat vel hendrerit et, efficitur malesuada libero. In ex mauris, faucibus et aliquet sed, varius a lacus. Nulla sit amet accumsan mi. Nam bibendum quis justo eget fermentum. Praesent ipsum ante, lacinia non urna quis, tristique condimentum velit. Pellentesque suscipit lectus lectus, in malesuada lacus maximus non. Duis libero dui, facilisis sagittis lobortis ac, luctus vitae ipsum. Quisque arcu neque, rhoncus et odio cursus, dictum aliquam nunc. Aenean et nisi at ex viverra auctor. Quisque varius felis dui, sit amet accumsan ante laoreet vel. Donec ipsum dui, porta in nunc nec, pellentesque iaculis dolor. In sit amet nisl fermentum, ultricies felis non, varius est.</Greyparagraph>
                    <Link>Meer over ons</Link>

            </Container>
        )
    }
}
