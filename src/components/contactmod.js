import React, { Component } from 'react'
import styled from "styled-components"
import { FaPhone}  from 'react-icons/fa'
import { FaEnvelope }  from 'react-icons/fa'
import { FaInstagram }  from 'react-icons/fa'



const Container = styled.div`
    ext-align: left;
    padding: 50px 0;
`
const Contactcont = styled.address`

`

const Contactline = styled.div`
 display: flex;
 
`
const ContactP = styled.p`
    font-size: 1.5em;
    display: inline-block;
    margin: 6px;
`
const Emptyspace = styled.span`
    width: 15px;
    height: 20px;
`

export default class Contactmod extends Component {
    render() {
        return (
            <Container>
                <h1>Contact</h1>
                <Contactcont>
                    <Contactline><ContactP>Duitsland Frieskring</ContactP></Contactline>
                    <Contactline><ContactP>Zomaareenstraat 12a</ContactP></Contactline>
                    <Contactline><ContactP>123 AB. Ergensstad</ContactP></Contactline>
                    <Contactline><ContactP>Nederland (Frl.)</ContactP></Contactline>
                    <Contactline><FaPhone className="contact-icon" /><Emptyspace></Emptyspace><ContactP>+31 12 34 56 789</ContactP></Contactline>
                    <Contactline><FaEnvelope className="contact-icon" /><Emptyspace></Emptyspace><ContactP>info@duitslandfrieskring.nl</ContactP></Contactline>
                </Contactcont>
            </Container>
        )
    }
}
