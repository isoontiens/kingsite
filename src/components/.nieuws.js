import React, { Component } from 'react'
import styled from "styled-components"
import Event from "./event"
import EventImg from "./event-img"

const Eventcontainer = styled.div`
display: inline-block;
width: 350px;
border: 2px solid lightgrey;
`

const Flex = styled.div`
    display: flex;
    justify-content: space-around;
    margin: 100px 0;
`

export default class Nieuws extends Component {
    render() {
        return (
            <div>
               <h1>Nieuws</h1>
               <Flex>
                 <Eventcontainer>
                 <Event />
                 </Eventcontainer>
                 <Eventcontainer>
                 <Event />
                 </Eventcontainer>
                </Flex>

            </div>
        )
    }
}
