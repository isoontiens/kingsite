import React, { Component } from 'react'
import styled from "styled-components"
import { FaFacebookSquare}  from 'react-icons/fa'
import { FaTwitterSquare}  from 'react-icons/fa'
import { FaInstagram }  from 'react-icons/fa'
import { FaYoutubeSquare}  from 'react-icons/fa'
import "../css/main-style.css"


const Container = styled.div`
  margin: 0 auto;

  display: flex;
  justify-content: center;
  height: 200px;

`
const Link = styled.a`

`


export default class share extends Component {
    render() {
        return (
            <Container>
               <Link><FaFacebookSquare className="sm-icon" /></Link>
               <Link><FaTwitterSquare className="sm-icon" /></Link>
               <Link><FaInstagram className="sm-icon" /></Link>
               <Link><FaYoutubeSquare className="sm-icon" /></Link>    
               
            </Container>
        )
    }
}
